"use strict";

const gBASE_URL_PIZZA =
  "https://64747ed27de100807b1b0fa4.mockapi.io/api/v1/pizza";
const gBASE_URL_BLOGS =
  "https://64747ed27de100807b1b0fa4.mockapi.io/api/v1/blogs";

const gBASE_URL_PIZZA_FILTER = `https://food-ordering-fvo9.onrender.com/api/foods`;

let vCurrentPage = 1;
let vPerPage = 5;
let vToTalPage = 0;

let vRatingArr = [];

$(document).ready(() => {
  displayPizzaToBrowser();
  updateCartColor();
});

async function getAllPizza() {
  const response = await fetch(gBASE_URL_PIZZA_FILTER);
  const data = await response.json();
  return data.rows;
}

async function displayPizzaToBrowser() {
  var vStart = (vCurrentPage - 1) * vPerPage;
  var vEnd = vStart + vPerPage;
  $("#pizza-container").empty();

  var vAllPizza = await getAllPizza();

  vAllPizza = filterPizza(vAllPizza);

  vToTalPage = Math.ceil(vAllPizza.length / vPerPage);

  if (vAllPizza.length !== 0) {
    vAllPizza.slice(vStart, vEnd).forEach((pizza) => {
      const html = `<div class="card-container mb-4 px-sm-2 px-5">
                      <div class="card">
                        <img src="${pizza.imageUrl}" class="card-img" />
                        <div class="card-body">
                          <div class="flex-layout">
                            <h3 class="pizza-name h5">${pizza.name}</h3>
                            <span class="ms-2 pizza-price">$${
                              pizza.price
                            }</span>
                          </div>
                          <div class="d-flex mt-2">
                            <div class="grey-box d-flex align-items-center">
                              <i class="fa-solid fa-star me-1"></i>
                              <span class="rating">${pizza.rating}</span>
                            </div>
                            <div class="grey-box me-auto ms-1 d-flex align-items-center">
                              <span class="pizza-time">${pizza.time}</span>
                            </div>
                            <button data-pizza='${JSON.stringify(
                              pizza
                            )}' class="btn yellow-box bg-warning rounded-1 px-2 py-1 add-pizza center-child">
                              <i class="fa-solid fa-plus text-white fw-6 font-5"></i>
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>`;
      $("#pizza-container").append(html);
    });
    populatePageNumber();
  } else {
    $("#pizza-container").append(
      `<h3 class='text-center mt-3 w-100'>Pizza Not Found</h3>`
    );
  }
  updateActiveItemNumber();
}

async function populatePageNumber() {
  vToTalPage === 0 && (await displayPizzaToBrowser());
  $("#page-number-container").empty();

  for (let index = 1; index <= vToTalPage; index++) {
    $("<li>")
      .addClass(`item-box`)
      .text(index)
      .appendTo(`#page-number-container`);
  }
}

function navigatePage(number) {
  var pageChange = true;
  if (number === -1 && vCurrentPage > 1) vCurrentPage--;
  else if (number === 1 && vCurrentPage < vToTalPage) vCurrentPage++;
  else if (typeof number === "string" && parseInt(number) !== vCurrentPage)
    vCurrentPage = parseInt(number);
  else pageChange = false;

  pageChange && displayPizzaToBrowser();
}

$("#btn-pre").click(() => navigatePage(-1));
$("#btn-next").click(() => navigatePage(1));

$("#page-number-container").on("click", ".item-box", function () {
  navigatePage($(this).text());
});

function updateActiveItemNumber() {
  $("#page-number-container")
    .children()
    .removeClass("active")
    .eq(vCurrentPage - 1)
    .addClass("active");

  $("#btn-next").toggleClass(`btn-disable`, vCurrentPage === vToTalPage);
  $("#btn-pre").toggleClass(`btn-disable`, vCurrentPage === 1);
}

function addProductIdToLocalStorage(paramPizzaObj) {
  var vPizzaContainer = JSON.parse(localStorage.getItem("Pizza"));

  var vId = JSON.parse(paramPizzaObj).id; // Get the Pizza Id that user add

  if (!vPizzaContainer) vPizzaContainer = {};

  if (!vPizzaContainer[vId])
    vPizzaContainer[vId] = {
      quantity: 0,
      pizza: paramPizzaObj,
    };

  vPizzaContainer[vId].quantity++;

  localStorage.setItem("Pizza", JSON.stringify(vPizzaContainer));
  console.log(vPizzaContainer);
}

// Handle Add Product and local storage
$("#cart").css("color", "white");

$("#pizza-container").on("click", ".add-pizza", function () {
  var vPizza = this.dataset.pizza;
  addProductIdToLocalStorage(vPizza);
  updateCartColor();
});

function updateCartColor() {
  if (localStorage.length > 0) {
    if (localStorage.getItem("Pizza") === "{}") return;
    $("#cart").addClass("text-warning");
  }
}

// Slider
let vSliderValue = [0, 30];
$("#slider-range").slider({
  range: true,
  values: [0, 30],
  min: 0,
  max: 30,
  step: 1,
  slide: function (event, ui) {
    $("#min-price").html(ui.values[0]);

    $("#max-price").html(ui.values[1]);
    vSliderValue = ui.values;
  },
});

// Filter Pizza

$("#btn-filter").click(() => {
  displayPizzaToBrowser();
});

function filterPizza(paramPizzaArr) {
  var vMaxPrice = vSliderValue[1];
  var vMinPrice = vSliderValue[0];
  var vPizzaFiltered = paramPizzaArr.filter(
    (pizza) => pizza.price <= vMaxPrice && pizza.price >= vMinPrice
  );
  return vRatingArr.length === 0
    ? vPizzaFiltered
    : vPizzaFiltered.filter((pizza) =>
        vRatingArr.includes(Math.floor(pizza.rating))
      );
}

$("#rating-box").on("click", ".checkbox", function () {
  var vStarValue = parseInt($(this).val());

  if ($(this).is(":checked")) {
    vRatingArr.push(vStarValue);
  } else {
    var index = vRatingArr.indexOf(vStarValue);
    vRatingArr.splice(index, 1);
  }
  displayPizzaToBrowser();
});
